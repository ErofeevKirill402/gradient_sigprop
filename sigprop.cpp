#include <iostream>
#include <algorithm>
#include <mutex>

#include <omp.h>

#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <Eigen/Eigenvalues>

#include <boost/progress.hpp>
#include <boost/multi_array.hpp>
#include <boost/range/numeric.hpp>

#include <vexcl/vexcl.hpp>
#include <viennacl/matrix.hpp>
#include <viennacl/linalg/lu.hpp>

#include "sigprop.h"

static std::once_flag viennacl_initialized;

const vex::Context& ctx() {
    static vex::Context context( vex::Filter::Exclusive(
                vex::Filter::Env             &&
                vex::Filter::DoublePrecision &&
                vex::Filter::Count(1)
                ) );
    return context;
}

//---------------------------------------------------------------------------
void signal_properties_cov_matrix(
        double *cov_dir,    // [sum(mask) x sum(mask)]
        double *cov_inv,    // [sum(mask) x sum(mask)]
        int nsen,
        int lmod,
        int lfld,
        const int    *mask, // [napp x nsen x lmod]
        const double *z     // [nsen x lfld]
        )
{
    using vex::element_index;
    using vex::raw_pointer;
    using vex::reduce;
    using vex::extents;

    std::cout << "nsen:    " << nsen << std::endl;
    std::cout << "lmod:    " << lmod << std::endl;
    std::cout << "lfld:    " << lfld << std::endl;

    std::cout << "GPU:     " << ctx().queue(0)          << "\n"
        << "Threads: " << omp_get_max_threads() << "\n"
        << std::endl;

    std::call_once(viennacl_initialized, []() {
            viennacl::ocl::setup_context(0,
                    ctx().context(0)(),
                    ctx().device(0)(),
                    ctx().queue(0)());
            });

    vex::profiler<> prof(ctx());

    vex::Reductor<double, vex::SUM_Kahan> sum(ctx());

    vex::vector<double> Z(ctx(), nsen * lfld, z);
    vex::vector<double> E(ctx(), nsen);

    prof.tic_cl("mean(Z)");
    E = reduce<vex::SUM>(extents[nsen][lfld], Z, 1) / lfld;
    prof.toc("mean(Z)");

    int nnz = std::count_if(mask, mask + nsen * lmod, [](int m){ return m; });

    std::vector<int> off; off.reserve(nnz);
    std::vector<int> sen; sen.reserve(nnz);

    boost::const_multi_array_ref<int, 2> M(mask, boost::extents[nsen][lmod]);
    for(int i = 0; i < nsen; ++i) {
        for(int j = 0; j < lmod; ++j) {
            if (M[i][j]) {
                off.push_back(j);
                sen.push_back(i);
            }
        }
    }

    vex::vector<int> Off(ctx(), off);
    vex::vector<int> Sen(ctx(), sen);

    VEX_FUNCTION(double, cov,
            (int, idx)(int, nnz)(int, lfld)
            (int*, sen)(int*, off)(double*, Z)(double*, E),

            int i = idx / nnz;
            int j = idx % nnz;

            int is = sen[i];
            int js = sen[j];

            int im = off[i];
            int jm = off[j];

            double Ei = E[is];
            double Ej = E[js];

            global double *Zi = Z + is * lfld;
            global double *Zj = Z + js * lfld;

            double sum = 0;
            for(int k = 0; k < lfld; ++k)
                sum += (Zi[(im + k) % lfld] - Ei) * (Zj[(jm + k) % lfld] - Ej);

            return sum / lfld;
            );

    prof.tic_cl("covariance");
    prof.tic_cl("assemble");
    vex::vector<double> C(ctx(), nnz * nnz);
    C = cov(element_index(), nnz, lfld, raw_pointer(Sen), raw_pointer(Off), raw_pointer(Z), raw_pointer(E));
    vex::copy(C.begin(), C.end(), cov_dir);
    prof.toc("assemble");

    prof.tic_cpu("invert");
    {
        vex::vector<double> I(ctx(), nnz * nnz);
        I = (element_index() / nnz == element_index() % nnz);

        viennacl::matrix<double> c(C(0).raw(), nnz, nnz);
        viennacl::matrix<double> ci(I(0).raw(), nnz, nnz);

        viennacl::linalg::lu_factorize(c);
        viennacl::linalg::lu_substitute(c, ci);

        C.swap(I);
    }
    prof.toc("invert");
    vex::copy(C.begin(), C.end(), cov_inv);
    prof.toc("covariance");

    std::cout << prof << std::endl;
}

//---------------------------------------------------------------------------
void signal_properties_cov_6(
        double *m,          // [napp x (lfld - lmod + 1) x 6]
        double *snr,        // [napp x (lfld - lmod + 1)]
        double *eval,       // [napp x (lfld - lmod + 1) x 3]
        double *evec,       // [napp x (lfld - lmod + 1) x 3 x 3]
        int napp,
        int nsen,
        int lmod,
        int lfld,
        const double *sxx,  // [napp x nsen x lmod]
        const double *syy,  // [napp x nsen x lmod]
        const double *szz,  // [napp x nsen x lmod]
        const double *sxy,  // [napp x nsen x lmod]
        const double *sxz,  // [napp x nsen x lmod]
        const double *syz,  // [napp x nsen x lmod]
        const int    *mask, // [napp x nsen x lmod]
        const double *z     // [nsen x lfld]
        )
{
    using vex::element_index;
    using vex::raw_pointer;
    using vex::reduce;
    using vex::extents;

    std::cout << "napp:    " << napp << std::endl;
    std::cout << "nsen:    " << nsen << std::endl;
    std::cout << "lmod:    " << lmod << std::endl;
    std::cout << "lfld:    " << lfld << std::endl;

    static vex::Context ctx( vex::Filter::Exclusive(
                vex::Filter::Env             &&
                vex::Filter::DoublePrecision &&
                vex::Filter::Count(1)
                ) );
    std::cout << "GPU:     " << ctx.queue(0)          << "\n"
              << "Threads: " << omp_get_max_threads() << "\n"
              << std::endl;

    static std::once_flag context_initialized;
    std::call_once(context_initialized, []() {
        viennacl::ocl::setup_context(0,
                ctx.context(0)(),
                ctx.device(0)(),
                ctx.queue(0)());
        });

    vex::profiler<> prof(ctx);

    vex::Reductor<double, vex::SUM_Kahan> sum(ctx);

    int n = nsen * lmod;

    vex::multivector<double, 6> S(ctx, napp * n);

    vex::copy(sxx, sxx + napp * n, S(0).begin());
    vex::copy(syy, syy + napp * n, S(1).begin());
    vex::copy(szz, szz + napp * n, S(2).begin());
    vex::copy(sxy, sxy + napp * n, S(3).begin());
    vex::copy(sxz, sxz + napp * n, S(4).begin());
    vex::copy(syz, syz + napp * n, S(5).begin());

    vex::vector<double> Z(ctx, nsen * lfld, z);

    // Compute inverse of covariance matrix
    prof.tic_cl("covariance");
    vex::vector<double> C(ctx, n * n);
    {
        prof.tic_cl("assemble");
        vex::vector<double> E(ctx, nsen);
        std::cout << "Computing mean(Z)..." << std::endl;

        E = reduce<vex::SUM>(extents[nsen][lfld], Z, 1) / lfld;

        std::cout << "Computing cov(Zi,Zj)..." << std::endl;

        VEX_FUNCTION(double, cov, (int, idx)(int, nsen)(int, lmod)(int, lfld)(double*, Z)(double*, E),
                int n = nsen * lmod;
                int i = idx / n;
                int j = idx % n;

                int is = i / lmod;
                int im = i % lmod;

                int js = j / lmod;
                int jm = j % lmod;

                double Ei = E[is];
                double Ej = E[js];

                global double *Zi = Z + is * lfld;
                global double *Zj = Z + js * lfld;

                double sum = 0;
                for(int k = 0; k < lfld; ++k)
                    sum += (Zi[(im + k) % lfld] - Ei) * (Zj[(jm + k) % lfld] - Ej);

                return sum / lfld;
                );

        C = cov(element_index(), nsen, lmod, lfld, raw_pointer(Z), raw_pointer(E));
        prof.toc("assemble");

        prof.tic_cpu("invert");
        std::cout << "Inverting covariance matrix..." << std::endl;
        vex::vector<double> I(ctx, n * n);
        I = (element_index() / n == element_index() % n);

        viennacl::matrix<double> c(C(0).raw(), n, n);
        viennacl::matrix<double> ci(I(0).raw(), n, n);

        viennacl::linalg::lu_factorize(c);
        viennacl::linalg::lu_substitute(c, ci);

        C.swap(I);
        prof.toc("invert");
    }
    prof.toc("covariance");

    // Do the work
    int lout = lfld - lmod + 1;
    prof.tic_cl("Find M");
    {
        vex::multivector<double, 6> CS(ctx, n);

        VEX_FUNCTION(double, get_cs, (int, idx)(int, n)(double*, C)(double*, S),
                int k = idx / n;
                int i = idx % n;

                C += i * n;
                S += k * n;

                double sum = 0;
                for(int j = 0; j < n; ++j)
                    sum += C[j] * S[j];

                return sum;
                );

        for(int i =0; i < 6; ++i)
            CS(i) = get_cs(element_index(), n, raw_pointer(C), raw_pointer(S(i)));

        VEX_FUNCTION(double, get_rhs,
                (int, idx)(int, lout)(int, nsen)(int, lmod)(int, lfld)
                (double*, CS0)(double*, CS1)(double*, CS2)
                (double*, CS3)(double*, CS4)(double*, CS5)
                (double*, Z),
                int n = nsen * lmod;
                int k = idx / (6 * lout);
                int t = idx % (6 * lout);

                global double *CS;
                switch(t % 6) {
                    case 0: CS = CS0; break;
                    case 1: CS = CS1; break;
                    case 2: CS = CS2; break;
                    case 3: CS = CS3; break;
                    case 4: CS = CS4; break;
                    case 5: CS = CS5; break;
                }
                t /= 6;

                CS += k * n;
                Z  += t;

                double sum = 0;
                for(int jsen = 0, j = 0; jsen < nsen; ++jsen) {
                    for(int jmod = 0; jmod < lmod; ++jmod, ++j) {
                        sum += CS[j] * Z[jsen * lfld + jmod];
                    }
                }

                return sum;
                );

        vex::vector<double> B(ctx, napp * lout * 6);
        B = get_rhs(element_index(), lout, nsen, lmod, lfld,
                raw_pointer(CS(0)), raw_pointer(CS(1)), raw_pointer(CS(2)),
                raw_pointer(CS(3)), raw_pointer(CS(4)), raw_pointer(CS(5)),
                raw_pointer(Z)
                );

        vex::copy(B.begin(), B.end(), m);

        boost::progress_display bar(napp, std::cout, "Computing results...\n");

        for(int k = 0; k < napp; ++k, m += 6 * lout, snr += lout, eval += 3 * lout, evec += 9 * lout, ++bar) {
            Eigen::Matrix<double,6,6> a, ainv;

            for(int l = 0; l < 6; ++l)
                for(int m = 0; m <= l; ++m)
                    a(l,m) = a(m,l) = sum(CS(l) * S(m));

            ainv = a.lu().inverse().eval();

            std::cout << a << std::endl;
            std::cout << ainv << std::endl;

#pragma omp parallel for
            for(int j = 0; j < lout; ++j) {
                Eigen::Map<Eigen::Matrix<double,6,1>> b(m + j * 6);

                b = ainv * b;

                snr[j] = b.transpose() * a * b;

                Eigen::Matrix3d M;
                M << b(0), b(3), b(4),
                     b(3), b(1), b(5),
                     b(4), b(5), b(2);
                Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigen(M);

                std::copy_n(eigen.eigenvalues().data(),  3, eval + j * 3);
                std::copy_n(eigen.eigenvectors().data(), 9, evec + j * 9);
            }
        }
    }
    prof.toc("Find M");

    std::cout << prof << std::endl;
}

//---------------------------------------------------------------------------
void signal_properties_var_6(
        double *m,         // [napp x (lfld - lmod + 1) x 6]
        double *snr,       // [napp x (lfld - lmod + 1)]
        double *eval,      // [napp x (lfld - lmod + 1) x 3]
        double *evec,      // [napp x (lfld - lmod + 1) x 3 x 3]
        int napp,
        int nsen,
        int lmod,
        int lfld,
        const double *sxx, // [napp x nsen x lmod]
        const double *syy, // [napp x nsen x lmod]
        const double *szz, // [napp x nsen x lmod]
        const double *sxy, // [napp x nsen x lmod]
        const double *sxz, // [napp x nsen x lmod]
        const double *syz, // [napp x nsen x lmod]
        const double *z    // [nsen x lfld]
        )
{
    using vex::element_index;
    using vex::raw_pointer;
    using vex::permutation;
    using vex::reduce;
    using vex::extents;
    using vex::tag;
    using vex::make_temp;

    std::cout << "napp: " << napp << std::endl;
    std::cout << "nsen: " << nsen << std::endl;
    std::cout << "lmod: " << lmod << std::endl;
    std::cout << "lfld: " << lfld << std::endl;

    std::cout << "GPU:  " << ctx().queue(0) << "\n\n";

    vex::profiler<> prof(ctx());

    vex::Reductor<double, vex::SUM> sum(ctx());

    int n = nsen * lmod;

    vex::multivector<double, 6> S(ctx(), napp * n);
    vex::copy(sxx, sxx + napp * n, S(0).begin());
    vex::copy(syy, syy + napp * n, S(1).begin());
    vex::copy(szz, szz + napp * n, S(2).begin());
    vex::copy(sxy, sxy + napp * n, S(3).begin());
    vex::copy(sxz, sxz + napp * n, S(4).begin());
    vex::copy(syz, syz + napp * n, S(5).begin());

    vex::vector<double> Z(ctx(), nsen * lfld, z);

    // Compute variance vector
    prof.tic_cl("variance");
    vex::vector<double> D(ctx(), nsen);
    {
        std::vector<double> d(nsen);

        for(int i = 0; i < nsen; ++i) {
            auto z = tag<1>(Z);
            double s1 = sum(permutation(element_index(i * lfld, lfld))(z));
            double s2 = sum(permutation(element_index(i * lfld, lfld))(z * z));

            d[i] = (s2 - s1 * s1 / lfld) / lfld;
        }

        vex::copy(d, D);
    }
    prof.toc("variance");

    // Do the work
    int lout = lfld - lmod + 1;
    prof.tic_cl("Find M");
    VEX_FUNCTION(double, get_rhs,
            (int, idx)(int, lout)(int, nsen)(int, lmod)(int, lfld)
            (double*, S0)(double*, S1)(double*, S2)
            (double*, S3)(double*, S4)(double*, S5)
            (double*, Z)(double*, D),
            int n = nsen * lmod;
            int k = idx / (6 * lout);
            int t = idx % (6 * lout);

            global double *S;
            switch(t % 6) {
                case 0: S = S0; break;
                case 1: S = S1; break;
                case 2: S = S2; break;
                case 3: S = S3; break;
                case 4: S = S4; break;
                case 5: S = S5; break;
            }
            t /= 6;

            S += k * n;
            Z += t;

            double sum = 0;
            for(int jsen = 0, j = 0; jsen < nsen; ++jsen) {
                for(int jmod = 0; jmod < lmod; ++jmod, ++j) {
                    sum += S[j] * Z[jsen * lfld + jmod] / D[jsen];
                }
            }

            return sum;
            );

    vex::vector<double> B(ctx(), napp * lout * 6);
    B = get_rhs(element_index(), lout, nsen, lmod, lfld,
            raw_pointer(S(0)), raw_pointer(S(1)), raw_pointer(S(2)),
            raw_pointer(S(3)), raw_pointer(S(4)), raw_pointer(S(5)),
            raw_pointer(Z), raw_pointer(D)
            );
    vex::copy(B.begin(), B.end(), m);

    vex::slicer<2> app(vex::extents[napp][n]);
    boost::progress_display bar(napp, std::cout);
    for(int k = 0; k < napp; ++k, ++bar) {
        Eigen::Matrix<double,6,6> a, ainv;

        for(int l = 0; l < 6; ++l)
            for(int m = 0; m <= l; ++m)
                a(l,m) = a(m,l) = sum(
                        app[k](S(l)) * app[k](S(m)) / permutation(element_index() / lmod)(D)
                        );

        ainv = a.lu().inverse().eval();

#pragma omp parallel for
        for(int j = 0; j < lout; ++j) {
            Eigen::Map<Eigen::Matrix<double,6,1>> b(m + j * 6);

            b = ainv * b;
            snr[j] = b.transpose() * a * b;

            Eigen::Matrix3d M;
            M << b(0), b(3), b(4),
                 b(3), b(1), b(5),
                 b(4), b(5), b(2);
            Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigen(M);

            std::copy_n(eigen.eigenvalues().data(),  3, eval + j * 3);
            std::copy_n(eigen.eigenvectors().data(), 9, evec + j * 9);
        }

        snr  += lout;
        m    += lout * 6;
        eval += lout * 3;
        evec += lout * 9;
    }
    prof.toc("Find M");

    std::cout << prof << std::endl;
}

//---------------------------------------------------------------------------
void signal_properties_cov_1(
        double *m,        // [napp x (lfld - lmod + 1)]
        double *a,        // [napp x (lfld - lmod + 1)]
        int napp,
        int nsen,
        int lmod,
        int lfld,
        const double *s,    // [napp x nsen x lmod]
        const int    *mask, // [napp x nsen x lmod]
        const double *z     // [nsen x lfld]
        )
{
    using vex::element_index;
    using vex::raw_pointer;
    using vex::reduce;
    using vex::extents;
    using vex::_;

    std::cout << "napp:    " << napp << std::endl;
    std::cout << "nsen:    " << nsen << std::endl;
    std::cout << "lmod:    " << lmod << std::endl;
    std::cout << "lfld:    " << lfld << std::endl;

    std::cout << "GPU:     " << ctx().queue(0)          << "\n"
              << "Threads: " << omp_get_max_threads() << "\n"
              << std::endl;

    std::call_once(viennacl_initialized, []() {
        viennacl::ocl::setup_context(0,
                ctx().context(0)(),
                ctx().device(0)(),
                ctx().queue(0)());
        });

    vex::profiler<> prof(ctx());

    vex::Reductor<double, vex::SUM_Kahan> sum(ctx());

    int lout = lfld - lmod + 1;
    int n = nsen * lmod;

    vex::vector<double> Z(ctx(), nsen * lfld, z);
    vex::vector<double> E(ctx(), nsen);
    vex::vector<double> B(ctx(), lout);

    prof.tic_cl("mean(Z)");
    E = reduce<vex::SUM>(extents[nsen][lfld], Z, 1) / lfld;
    prof.toc("mean(Z)");

    boost::progress_display bar(napp, std::cout);
    for(int app = 0; app < napp; ++app, ++bar) {
        boost::const_multi_array_ref<double, 2> S(s + app * n, boost::extents[nsen][lmod]);
        boost::const_multi_array_ref<int,    2> M(mask + app * n, boost::extents[nsen][lmod]);

        std::vector<int> ptr(nsen + 1, 0);
        for(int i = 0; i < nsen; ++i)
            for(int j = 0; j < lmod; ++j)
                if (M[i][j]) ++ptr[i + 1];

        boost::partial_sum(ptr, ptr.begin());

        int nnz = ptr.back();

        std::vector<int> off; off.reserve(nnz);
        std::vector<int> sen; sen.reserve(nnz);

        std::vector<double> s0; s0.reserve(nnz);

        for(int i = 0; i < nsen; ++i) {
            for(int j = 0; j < lmod; ++j) {
                if (M[i][j]) {
                    off.push_back(j);
                    sen.push_back(i);

                    s0.push_back(S[i][j]);
                }
            }
        }

        vex::vector<int> Ptr(ctx(), ptr);
        vex::vector<int> Off(ctx(), off);
        vex::vector<int> Sen(ctx(), sen);

        vex::vector<double> S0(ctx(), s0);

        VEX_FUNCTION(double, cov,
                (int, idx)(int, nnz)(int, lfld)
                (int*, sen)(int*, off)(double*, Z)(double*, E),

                int i = idx / nnz;
                int j = idx % nnz;

                int is = sen[i];
                int js = sen[j];

                int im = off[i];
                int jm = off[j];

                double Ei = E[is];
                double Ej = E[js];

                global double *Zi = Z + is * lfld;
                global double *Zj = Z + js * lfld;

                double sum = 0;
                for(int k = 0; k < lfld; ++k)
                    sum += (Zi[(im + k) % lfld] - Ei) * (Zj[(jm + k) % lfld] - Ej);

                return sum / lfld;
                );

        prof.tic_cl("covariance");
        prof.tic_cl("assemble");
        vex::vector<double> C(ctx(), nnz * nnz);
        C = cov(element_index(), nnz, lfld, raw_pointer(Sen), raw_pointer(Off), raw_pointer(Z), raw_pointer(E));
        prof.toc("assemble");

        prof.tic_cpu("invert");
        {
            vex::vector<double> I(ctx(), nnz * nnz);
            I = (element_index() / nnz == element_index() % nnz);

            viennacl::matrix<double> c(C(0).raw(), nnz, nnz);
            viennacl::matrix<double> ci(I(0).raw(), nnz, nnz);

            viennacl::linalg::lu_factorize(c);
            viennacl::linalg::lu_substitute(c, ci);

            C.swap(I);
        }
        prof.toc("invert");

        prof.toc("covariance");

        prof.tic_cl("CS");
        vex::vector<double> CS0(ctx(), nnz);
        vex::slicer<2> mat(extents[nnz][nnz]);
        vex::slicer<1> vec(extents[nnz]);
        CS0 = vex::tensordot(mat[_](C), vec[_](S0), vex::axes_pairs(1, 0));
        prof.toc("CS");

        prof.tic_cl("A");
        a[app] = sum(CS0 * S0);
        prof.toc("A");

        if (a[app] == 0) {
            std::fill_n(m, lout, 0.0);
        } else {
            double ainv = 1 / a[app];

            prof.tic_cl("rhs");
            VEX_FUNCTION(double, get_rhs,
                    (int, idx)(int, nsen)(int, lfld)(int*, ptr)(int*, off)
                    (double*, CS)(double*, Z),

                    Z += idx;

                    double sum = 0;
                    int end = 0;
                    for(int jsen = 0, j = 0; jsen < nsen; ++jsen) {
                        int beg = end; end = ptr[jsen + 1];

                        for(int jmod = beg; jmod < end; ++jmod, ++j) {
                            sum += CS[j] * Z[jsen * lfld + off[jmod]];
                        }
                    }

                    return sum;
                    );

            B = get_rhs(element_index(), nsen, lfld,
                    raw_pointer(Ptr), raw_pointer(Off),
                    raw_pointer(CS0), raw_pointer(Z)
                    );
            vex::copy(B.begin(), B.end(), m);
            prof.toc("rhs");

#pragma omp parallel for
            for(int j = 0; j < lout; ++j) {
                m[j] *= ainv;
            }
        }

        m += lout;
    }

    std::cout << prof << std::endl;
}

//---------------------------------------------------------------------------
void signal_properties_var_1(
        double *m,       // [napp x (lfld - lmod + 1)]
        double *a,       // [napp x (lfld - lmod + 1)]
        int napp,
        int nsen,
        int lmod,
        int lfld,
        const double *s, // [napp x nsen x lmod]
        const double *z  // [nsen x lfld]
        )
{
    using boost::extents;

    int lout = lfld - lmod + 1;

    std::cout << "napp: " << napp << std::endl;
    std::cout << "nsen: " << nsen << std::endl;
    std::cout << "lmod: " << lmod << std::endl;
    std::cout << "lfld: " << lfld << std::endl;
    std::cout << "lout: " << lout << std::endl;

    boost::const_multi_array_ref<double, 3> S(s, extents[napp][nsen][lmod]);
    boost::const_multi_array_ref<double, 2> Z(z, extents[nsen][lfld]);
    boost::multi_array_ref<double, 2>       M(m, extents[napp][lout]);

    // Compute variance vector
    std::vector<double> D(nsen);
#pragma omp parallel for schedule(static,1)
    for(int i = 0; i < nsen; ++i) {
        double s1 = 0;
        double s2 = 0;

        for(int j = 0; j < lfld; ++j) {
            double cz = Z[i][j];

            s1 += cz;
            s2 += cz * cz;
        }

        D[i] = (s2 - s1 * s1 / lfld) / lfld;
    }

    // Do the work
    {
        for(int k = 0; k < napp; ++k) {
            double A = 0;
            for(int i = 0; i < nsen; ++i)
                for(int j = 0; j < lmod; ++j)
                    A += S[k][i][j] * S[k][i][j] / D[i];

            a[k] = A;

#pragma omp parallel for
            for(int t = 0; t < lout; ++t) {
                double sum = 0;
                for(int i = 0; i < nsen; ++i)
                    for(int j = 0; j < lmod; ++j)
                        sum += S[k][i][j] * Z[i][j + t] / D[i];

                M[k][t] = sum / A;
            }
        }
    }
}

