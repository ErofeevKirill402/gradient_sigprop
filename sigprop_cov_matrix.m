function [C, Ci] = sigprop_cov_matrix(Msk, Z)
    lib = 'libsigprop';
    if not(libisloaded(lib))
        loadlibrary(lib, 'sigprop.h');
    end

    [nsen, lmod] = size(Msk);
    lfld = size(Z, 2);

    nnz = sum(Msk(:) > 0);
    C   = zeros([nnz, nnz]);
    Ci  = zeros([nnz, nnz]);

    [C, Ci] = calllib(lib, 'signal_properties_cov_matrix', ...
        C, Ci, nsen, lmod, lfld, Msk', Z' ...
        );
