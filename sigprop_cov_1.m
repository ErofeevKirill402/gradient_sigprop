function [M, A] = sigprop_cov_1(S, Msk, Z)
    lib = 'libsigprop';
    if not(libisloaded(lib))
        loadlibrary(lib, 'sigprop.h');
    end

    [napp, nsen, lmod] = size(S);
    lfld = size(Z, 2);

    lout = lfld - lmod + 1;

    M = zeros(lout, napp);
    A = zeros(napp, 1);

    [M, A] = calllib(lib, 'signal_properties_cov_1', ...
        M, A, ...
        napp, nsen, lmod, lfld, ...
        permute(S,   [3, 2, 1]), ...
        permute(Msk, [3, 2, 1]), ...
        reshape(Z', 1, []) ...
        );

    M = M';
