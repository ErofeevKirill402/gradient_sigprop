function [M, A] = sigprop_var_1(S, Z)
    lib = 'libsigprop';
    if not(libisloaded(lib))
        loadlibrary(lib, 'sigprop.h');
    end

    [napp, nsen, lmod] = size(S);
    lfld = size(Z, 2);

    lout = lfld - lmod + 1;

    M = zeros(lout, napp);
    A = zeros(napp, 1);

    [M, A] = calllib(lib, 'signal_properties_var_1', M, A, ...
        napp, nsen, lmod, lfld, ...
        reshape(permute(S, [3, 2, 1]), 1, []), reshape(Z', 1, []));
    M = M';
