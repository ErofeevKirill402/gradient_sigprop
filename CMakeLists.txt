cmake_minimum_required(VERSION 2.8)
project(sigprop)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

#----------------------------------------------------------------------------
# Find python and boost
#----------------------------------------------------------------------------
find_package(PythonInterp)
find_package(PythonLibs ${PYTHON_VERSION_STRING})
find_package(NumPy)

if (PYTHON_VERSION_MAJOR EQUAL 3)
    set(BOOST_PYTHON_COMPONENT python3 CACHE STRING "Boost.Python library name")
else()
    set(BOOST_PYTHON_COMPONENT python CACHE STRING "Boost.Python library name")
endif()

find_package(Boost COMPONENTS
    date_time
    filesystem
    system
    program_options
    ${BOOST_PYTHON_COMPONENT}
    )

include_directories(
    ${Boost_INCLUDE_DIRS}
    ${PYTHON_INCLUDE_DIRS}
    ${NUMPY_INCLUDE_DIRS}
    )

#----------------------------------------------------------------------------
# Find Eigen
#----------------------------------------------------------------------------
find_path(EIGEN_INCLUDE Eigen/SparseCore PATH_SUFFIXES eigen3)
if (EIGEN_INCLUDE)
    include_directories(${EIGEN_INCLUDE})
    add_definitions(-DAMGCL_HAVE_EIGEN)
endif()

#----------------------------------------------------------------------------
# Find VexCL
#----------------------------------------------------------------------------
set(VEXCL_ROOT $ENV{VEXCL_ROOT} CACHE STRING "VexCL root")
if (VEXCL_ROOT)
    include_directories( ${VEXCL_ROOT} )

    find_package(OpenCL REQUIRED)
    include_directories( ${OPENCL_INCLUDE_DIRS} )
    set(VEXCL_BACKEND_LIBRARIES ${OPENCL_LIBRARIES})
    add_definitions(-DVEXCL_BACKEND_OPENCL)

    message(STATUS "Found VexCL at ${VEXCL_ROOT}")
    include_directories( ${VEXCL_ROOT} )
endif()

#----------------------------------------------------------------------------
# Find ViennaCL
#----------------------------------------------------------------------------
set(VIENNACL_ROOT $ENV{VIENNACL_ROOT} CACHE STRING "ViennaCL root")
if (VIENNACL_ROOT)
    include_directories( ${VIENNACL_ROOT} )
    add_definitions(-DVIENNACL_WITH_OPENCL)

    message(STATUS "Found ViennaCL at ${VIENNACL_ROOT}")
    include_directories( ${VIENNACL_ROOT} )
endif()

#----------------------------------------------------------------------------
# Find OpenMP
#----------------------------------------------------------------------------
find_package(OpenMP)
if (OpenMP_CXX_FLAGS)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif ()

#----------------------------------------------------------------------------
# Enable C++11 support
#----------------------------------------------------------------------------
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wno-unused-local-typedefs -Wno-deprecated-declarations -Wno-ignored-attributes -Wno-misleading-indentation")
add_definitions(-DVEXCL_MAX_ARITY=20)
add_definitions(-DBOOST_PROTO_MAX_ARITY=20)

#----------------------------------------------------------------------------
set(BUILD_SHARED_LIBS ON)
add_library(pysigprop sigprop.cpp pywrap.cpp)
target_link_libraries(pysigprop
    ${VEXCL_BACKEND_LIBRARIES}
    ${Boost_LIBRARIES}
    )
set_target_properties(pysigprop PROPERTIES PREFIX "")

#----------------------------------------------------------------------------
find_package(Boost COMPONENTS
    date_time
    filesystem
    system
    )

set(BUILD_SHARED_LIBS ON)
add_library(sigprop sigprop.cpp)
target_link_libraries(sigprop
    ${VEXCL_BACKEND_LIBRARIES}
    ${Boost_LIBRARIES}
    )

#----------------------------------------------------------------------------
foreach(script
        sigprop txt2h5.py sigprop.h
        sigprop_cov_6.m sigprop_var_6.m
        sigprop_cov_1.m sigprop_var_1.m
        test_matlab.m
        )
    configure_file(
        ${CMAKE_CURRENT_SOURCE_DIR}/${script}
        ${CMAKE_CURRENT_BINARY_DIR}/${script}
        COPYONLY
        )
endforeach()

#----------------------------------------------------------------------------
add_subdirectory(doc)
